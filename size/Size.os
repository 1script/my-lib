
Функция РазмерФайлов(Путь, ЕдИзм="") Экспорт

	Файл = Новый Файл(Путь);
	Если НЕ Файл.Существует() Тогда
		Возврат Неопределено;
	КонецЕсли;

	ОбщийРазмер = 0;

	Если Файл.ЭтоФайл() Тогда
		
		ОбщийРазмер = Файл.Размер();
	
	ИначеЕсли Файл.ЭтоКаталог() Тогда
		
		ВсеФайлы = НайтиФайлы(Путь, ПолучитьМаскуВсеФайлы(), Истина);
		Для каждого Файл Из ВсеФайлы Цикл
			Если Файл.ЭтоКаталог() Тогда
				Продолжить;
			КонецЕсли;
			ОбщийРазмер = ОбщийРазмер + Файл.Размер();
		КонецЦикла;
		
		Возврат ВыразитьВЕдиницих(ОбщийРазмер, ЕдИзм);

	Иначе
		
		ВызватьИсключение "Что ты есть такое?";
	
	КонецЕсли;

КонецФункции

Функция ВыразитьВЕдиницих(Знач Размер, Знач ЕдИзм) Экспорт
	
	ЕдИзм = ТРег(ЕдИзм);

	Если ЕдИзм = "B" Тогда
		ПорядокЧисла = 0;
	ИначеЕсли ЕдИзм = "Kb" Тогда
		ПорядокЧисла = 1;
	ИначеЕсли ЕдИзм = "Mb" Тогда
		ПорядокЧисла = 2;
	ИначеЕсли ЕдИзм = "Gb" Тогда
		ПорядокЧисла = 3;
	ИначеЕсли ЕдИзм = "H" Тогда //Human
		ПорядокЧисла = Мин(ПорядокЧисла(Размер, 1024), 3);
	Иначе
		Возврат Размер;
	КонецЕсли;

	ПредставлениеРазмера = НормализоватьВид(Размер, ПорядокЧисла);
	ЕдИзм = ПредставлениеПорядка(ПорядокЧисла);

	Возврат ПредставлениеРазмера + " " + ЕдИзм;

КонецФункции

Функция НормализоватьВид(Число, Порядок)
	Если Порядок = 0 Тогда
		Формат = "ЧДЦ=0; ЧГ=3,0";
	Иначе
		Формат = "ЧДЦ=1; ЧГ=3,0";
	КонецЕсли;

	Возврат Формат(Число/pow(1024,Порядок), Формат);
КонецФункции

Функция ПорядокЧисла(Знач Число, Знаменатель)

	Если Число/Знаменатель < 1 Тогда
		Возврат 0;
	Иначе
		Возврат 1 + ПорядокЧисла(Число/Знаменатель, Знаменатель);
	КонецЕсли;

КонецФункции

Функция ПредставлениеПорядка(Порядок)

	Представления = "b,Kb,Mb,Gb";

	Возврат СтрРазделить(Представления, ",")[Порядок];

КонецФункции
